package main

import (
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"net/http"
	"url/appname"
)

const STATIC_ROOT = http.Dir("./")

func main() {
	appname.NewConfig(configPath)

	router := mux.NewRouter()
	// urls:
	appname.Urls(router)

	if appname.GlobalConfig.Debug {
		// serve static files
		staticfiles := negroni.NewStatic(STATIC_ROOT)

		n := negroni.New(negroni.NewRecovery(), negroni.NewLogger(), staticfiles)
		n.UseHandler(router)
		n.Use(staticfiles)

		n.Run(":8000")
	} else {

	}
}
