package appname

import (
	"code.google.com/p/gcfg"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"io/ioutil"
	"log"
	"net"
	"net/smtp"
	"os"
	"strings"
)

type Config struct {
	Server struct {
		Debug          bool
		Hostname       string
		Port           int
		Workers        int
		Buffer         int
		Certificate    string
		Maxconnections int
	}

	Sessions struct {
		Timeout int
	}

	Crypto struct {
		PublicKey  string
		PrivateKey string
		// filled by reading the contents at the Public/Private Key paths.
		PublicKeyPem  []byte
		PrivateKeyPem []byte
		PublicKeyRSA  *rsa.PublicKey
		PrivateKeyRSA *rsa.PrivateKey
	}

	Db struct {
		Hostaddress string
		Host        string
		Port        int
		User        string
		Password    string
		Name        string
		Timeout     int
		// generated from the above properties
		ConnectionString string
	}

	Log struct {
		ErrorLog          string
		InfoLog           string
		Admins            string
		AdminEmailTo      []string
		EmailUseTls       string
		EmailUseSsl       string
		EmailHost         string
		EmailHostUser     string
		EmailHostPassword string
		EmailPort         string
		DefaultFromEmail  string
		SmtpAuth          smtp.Auth
	}
}

var GlobalConfig *Config

// NewConfig will load into GlobalConfig
// WHICH IS USED EVERYWHERE
// don't run this function blindly
func NewConfig(path string) *Config {
	cfg := new(Config)

	err := gcfg.ReadFileInto(cfg, path)
	if err != nil {
		fmt.Fprintln(os.Stdout, "Error parsing config files: %i", err)
	}

	// save the connection string
	if len(cfg.Db.Host) > 0 && len(cfg.Db.Hostaddress) == 0 {
		ipAddresses, err := net.LookupHost(cfg.Db.Host)
		if err != nil {
			log.Println("Couldn't lookup host", cfg.Db.Host, "\n", err)
		}
		if len(ipAddresses) > 0 {
			cfg.Db.Hostaddress = ipAddresses[0]
		}
	}
	cfg.Db.ConnectionString = fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s connect_timeout=%d",
		cfg.Db.Hostaddress, cfg.Db.Port, cfg.Db.Name, cfg.Db.User, cfg.Db.Password, cfg.Db.Timeout)

	// setup email error logs
	smtpAuth := smtp.PlainAuth("", cfg.Log.EmailHostUser, cfg.Log.EmailHostPassword, cfg.Log.EmailHost)
	cfg.Log.SmtpAuth = smtpAuth

	cfg.Log.AdminEmailTo = strings.Split(cfg.Log.Admins, ",")

	// make this available app wide
	GlobalConfig = cfg
	return cfg
}

func (c *Config) ConnectToDb() (*sql.DB, error) {
	return sql.Open("postgres", c.Db.ConnectionString)
}
