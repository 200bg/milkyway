package appname

import (
	// "fmt"
	"github.com/codegangsta/negroni"
	"github.com/gorilla/mux"
	"net/http"
)

const STATIC_ROOT = http.Dir("./")

func Urls(router *mux.Router) {
	router.HandleFunc("/", appname.HomeView)
}
