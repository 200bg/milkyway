# Milky Way Framework

*Just started. Not even functional.*

*Barely* a framework. More of a collection of technologies in a quick script.

Meant to be a javascript system with supporting backends that use JSON and nginx
for smooth client-side apps.

## Dependencies

### For Go
##### Routes
github.com/gorilla/mux
##### Config files
code.google.com/p/gcfg
##### Templating Server-side
github.com/flosch/pongo2
##### Database -- preferred, not required
github.com/lib/pq

##### Templating Client-side -- not in go
https://github.com/mozilla/nunjucks

##### For debug serving of static files
github.com/codegangsta/negroni
