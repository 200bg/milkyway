package main

import (
	"bitbucket.org/200bg/milkyway"
	"bitbucket.org/200bg/milkyway/fs"
	"flag"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func StartCommand(projectType string, appName string) {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	sourceDir := filepath.Join(dir, "/templates/", projectType)
	wd, err := os.Getwd()
	destinationDir := wd

	var appUrl string
	if projectType == "go" {
		appUrl = appName
		appName = filepath.Base(appUrl)
	}
	// based on projectType(go, node, flask, etc.)
	err = fs.CopyDir(sourceDir, destinationDir)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Copied template %s for \"%s\"...\n", sourceDir, projectType)
	}

	switch {
	case projectType == "go":

		files := []string{
			filepath.Join(wd, "/appname/config.go"),
			filepath.Join(wd, "/appname/urls.go"),
			filepath.Join(wd, "/appname/views.go"),
			filepath.Join(wd, "/main.go"),
		}
		for i := 0; i < len(files); i++ {
			fp := files[i]
			fileContents, err := ioutil.ReadFile(fp)
			// replace package references
			data := strings.Replace(fileContents, "url/appname", appUrl)
			data = strings.Replace(data, "appname", appName)

			ioutil.WriteFile(fp, data, 0755)
		}
	}

	// copy nginx config
	// copy app config
	sourceDir = filepath.Join(dir, "/templates/config")
	destinationDir = filepath.Join(wd, "/config")
	err = fs.CopyDir(sourceDir, destinationDir)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Copied config %s for \"%s\"...\n", sourceDir, projectType)
	}
	// static folder
	sourceDir = filepath.Join(dir, "/templates/static")
	destinationDir = filepath.Join(wd, "/static")
	err = fs.CopyDir(sourceDir, destinationDir)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("Copied static base files %s for \"%s\"...\n", sourceDir, projectType)
	}
}

func main() {
	if len(os.Args) == 1 {
		fmt.Println("Required argument: command")
		os.Exit(1)
	}

	command := flag.Arg(1)

	switch {
	case command == "":
		if len(os.Args) < 3 {
			fmt.Println("Required arguments for command: projectType appname")
			fmt.Printf("eg: %s go bitbucket.org/200bg/site\n", flag.Arg(0))
			os.Exit(1)
		}
		if len(os.Args) < 4 {
			fmt.Println("Required arguments for command: appname")
			fmt.Printf("eg: %s go bitbucket.org/200bg/site\n", flag.Arg(0))
			os.Exit(1)
		}
		StartCommand(flag.Arg(2), flag.Arg(3))
	}
}
